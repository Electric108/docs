---
displayed_sidebar: groupFederate
description: >
    Read the proposal for the Federate group.
last_update:
    author: Carlos Valdez
    date: March 30 2024
---
# `zot.social` Proposal

:::note

This is just a proposal. No official action has been taken.

:::

The Social group of the Ojos Project would be in charge of maintaining the
`zot.social` Mastodon instance.

`zot.social` would be a Mastodon instance in which UCI students would be
maintaining for UCI students, faculty, and staff to interact with the Fediverse.

## What would `zot.social` do differently?

For one, it can introduce UCI students to the concept of the Fediverse. It can
also be an "intro to open-source" project where we introduce people to working
with open-source projects.

We can promote `zot.social` as an alternative to using
[Threads](https://threads.net) without the algorithmic app, but rather with a
chronological ordered app that doesn't beg for your attention.

## What would maintainers do?

In theory, they would manage the CSS code and add/manage features, such as only
allowing people with `@uci.edu` emails to join.
