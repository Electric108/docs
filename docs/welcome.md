---
slug: /
displayed_sidebar: default
description: >
    Welcome to the Ojos Project documentation! This is a collection of Markdown
    files organized to help you get a better understanding of us as a team and
    our work.
last_update:
    author: Carlos Valdez
    date: April 2 2024
---
# Welcome

Welcome to the Ojos Project documentation! This is a collection of Markdown
files organized to help you get a better understanding of us as a team and our
work.

## What do these docs have?

As of now, these docs have instructions for our team. We will also include
important documents such as requirements analysis, and design guides.

## Contributing

If you would like to contribute, you are more than welcomed to. Go ahead and
read the instructions in the
[GitLab repository](https://gitlab.com/ojosproject/docs) to get started.

For easier access, you can click `Edit this button` to immediately open the
page using the [GitLab IDE](/url/developers/guides/gitlab-ide).

## Need help?

If anything in these documents do not make sense, please contact Carlos Valdez
any way you know how, or via email at
[cvaldezh@uci.edu](mailto:cvaldezh@uci.edu). You can also
[open an issue on GitLab](https://gitlab.com/ojosproject/docs/-/issues). Thanks!
