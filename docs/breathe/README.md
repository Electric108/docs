---
displayed_sidebar: groupBreathe
description: >
    Learn more about the Breathe Group at the Ojos Project.
last_update:
    author: Carlos Valdez
    date: March 28 2024
---
# Breathe Group

The Breathe "Group" (technically, it's just Carlos) is a section of the Ojos
Project where we build the You Are Typing web app.

The app basically initiates a conversation between the user and themselves. The
POV of the user shifts back and fourth so that the user can have a conversation
with themselves.

More information about the group's goal can be found on the [You Are Typing
Proposal](/breathe/proposal/).
