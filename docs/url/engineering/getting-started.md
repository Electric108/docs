---
displayed_sidebar: groupUrlTeamEngineering
description: >
    Get started with the Engineering team at the Ojos Project!
last_update:
    author: Carlos Valdez
    date: March 21, 2024
---
# Getting Started in the Engineering Team
