---
displayed_sidebar: groupUrlTeamEngineering
description: >
    The Engineering team of the Ojos Project is in charge of physically building
    the device. They interact with the Raspberry Pi and potentially contribute
    to the code, if needed.
last_update:
    author: Carlos Valdez
    date: March 21, 2024
---

# Engineering Team

This document is still being written.

<!-- TODO: Write out the responsibilities of the team. -->
<!-- TODO: Write out the tools you will be using. -->
<!-- TODO: Create a `getting-started.md` file for newcomers to the team. -->
<!-- TODO: Change the `author` and `date` values above. -->

<!-- ! Please add any new files or folders you want to add under the -->
<!-- ! engineering folder. Do not touch any other files. Thank you! -->

<!-- A good reference for the structure is the Research team homepage. -->
<!-- https://docs.ojosproject.org/teams/research/ -->
