---
displayed_sidebar: groupUrl
description: >
    These are the members of the Ojos Project URL Group.
last_update:
    author: Carlos Valdez
    date: March 31 2024
---
# Members of the URL Group

These are the people who have participated in the URL Group at the Ojos Project.

## Current Members

| Photo | Name                 |       Joined | Email                | Website                                                 |
| ----- | -------------------- | -----------: | -------------------- | ------------------------------------------------------- |
| ![Carlos' Gravatar](https://gravatar.com/avatar/41bb2938e02bf5326eb6b82ec02d919ca97cf68b376c4c5769fbba4acc85a190?s=150&d=mp) | Carlos Valdez        | October 2023 | cvaldezh@uci.edu     | https://calejvaldez.com                                 |
| ![Joseph's Gravatar](https://gravatar.com/avatar/fac497a877e467035b306a201b938608120228662480b08e0477c59b4cfe347a?s=150&d=mp) | Joseph Sweatt        | October 2023 | jsweatt@uci.edu      | [LinkedIn](https://www.linkedin.com/in/josephsweattjr/) |
| ![Ayush's Gravatar](https://gravatar.com/avatar/e4c598f9826093429edec4c38d6bfb93968a59b512259b07e7d1ffe4ff7bb115?s=150&d=mp) | Ayush Jain           | January 2024 | ayushj4@uci.edu      | https://ayushdotjain.com                                |
| ![Mark's Gravatar](https://gravatar.com/avatar/84ea4715fcf218179a00bd36150884d878b5d4a19fb731b2d09ab0eee7aba509?s=150&d=mp) | Mark S. Baldwin, PhD | October 2023 | baldwinm@ics.uci.edu | https://markbaldw.in                                    |

## Former Members

| Photo | Name             | Email                          | Website                                        | Contributions |
| ----- | ---------------- | ------------------------------ | ---------------------------------------------- | ------------- |
| ![Isabella's Gravatar](https://gravatar.com/avatar/e4b66681a801324cb8b596783892b45e275d0c182789cb22a2af0c14ff73f0f8?s=150&d=mp) | Isabella Gronich | igronich@students.imperial.edu | [Email](mailto:igronich@students.imperial.edu) | Researched various academic papers for the Ojos Project. |
