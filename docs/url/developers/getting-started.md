---
displayed_sidebar: groupUrlTeamDevelopers
last_update:
    author: Carlos Valdez
    date: March 21, 2024
---
# Getting Started in the Developers Team

Welcome to the Ojos Project's Developers team! We're excited to have you! These are
instructions for people who are just joining the team.

## GitLab

We have a [GitLab organization](https://gitlab.com/ojosproject/)! In it, we have
all of our repos and documentation for the Ojos Project. It may be useful to
join it.

### SSH Setup

To connect to GitLab, you will be required to use SSH. Please read
[SSH Setup](/url/developers/guides/ssh-setup/) to get started.

## Unix-like Environments

The Developers team will primarily use Unix-like operating systems. This includes
macOS, Linux, or the Windows Subsystem for Linux. If you don't use macOS or
Linux, please read
[Installing the Windows Subsystem for Linux](/url/developers/guides/installing-wsl/).

## GPG & Encryption Keys

The Ojos team is currently doing requirements analysis, and part of that includes
conducting interviews. For the security of the interviewees, we encrypt the
content of the interviews. Authorized Ojos team members are given access to these
encryption keys.

You must also install `gpg`. The install process for that, as well as
instructions for decrypting the interviews, is here:
[Decrypting the Interviews](/url/developers/guides/decrypt-interviews/)

## Admin Checklist

This checklist is here for the people helping you get set up.

- [ ] Ensure they are in the GitLab organization
  - [ ] Ensure they can connect to GitLab with SSH
- [ ] Ensure they have a Unix-like environment
- [ ] Provide access to encryption keys, if needed
  - [ ] Ensure they can decrypt interviews
