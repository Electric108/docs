---
displayed_sidebar: groupUrl
description: >
    Learn more about the URL Group at the Ojos Project.
last_update:
    author: Carlos Valdez
    date: March 28 2024
---
# URL Group

The [Undergraduate Research Lab](https://markbaldw.in/url/) is a research lab by
[Dr. Mark S. Baldwin](https://www.informatics.uci.edu/explore/faculty-profiles/mark-baldwin/)
at the [University of California, Irvine](https://uci.edu).

The goal of the URL Group at the Ojos Project is to develop a hospice device
that will make hospice patients more independent while helping caregivers care
for their loved one.

## Teams

There's currently three available teams:

- [Developers](/url/developers/) (led by Carlos)
- [Engineering](/url/engineering/) (led by Joseph)
- [Research](/url/research/)

For more information, please go to each team's respective link.
