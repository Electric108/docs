---
displayed_sidebar: groupUrl
description: >
    Welcome to the Ojos Project! This document is here to help you get set up
    so that you can work with us. We can't wait to see what you do here!
last_update:
    author: Carlos Valdez
    date: April 2 2024
---
# Welcome to the Ojos Project

We're pleased that you decided to join us. There's a few things we need you to
do to get started. ✨

## Figure out your commitment

Some students in this research project are enrolled in IN4MATX 199 at UCI. Read
the [IN4MATX 199 Acknowledgement](/policies/inf199-acknowledgement) to learn
more.

**If you are enrolled in IN4MATX 199, you are required to participate for a
minimum of two quarters.**

If you are not enrolled in IN4MATX 199, you may join and leave the project as
you'd like, with limits. Read
[Getting recognized as a member](#getting-recognized-as-a-member) below to learn
more.

## Add meetings to your calendar

On Thursdays at 12pm, we have a meeting with our Research Advisor,
[Dr. Mark S. Baldwin](https://www.informatics.uci.edu/explore/faculty-profiles/mark-baldwin/).
People are are enrolled **must**
attend this meeting.

We discuss our progress and the overall direction of where the project is
heading.

We also have other meetings, but that depends on the team you are in.

| Team             | Meeting           | Location | Description         |
| ---------------- | ----------------- | -------- | ------------------- |
| Developers Team  | Wed/Th @ 12pm-3pm | DBH 5089 | Work on the project |
| Engineering Team | Wed/Th @ 12pm-3pm | DBH 5089 | Work on the project |

## Join our Google Drive

We have an
[internal Google Drive](https://drive.google.com/drive/folders/1nsghXOEXTWsKTtgMzlCuXMp8iIiq8iBb)
that hosts some important files we use. It'll be helpful to join that!

## Join our Slack workspace

We have a [Slack workspace](https://ojosproject.slack.com) that we use to
communicate with each other. Please join!

## Send us your email

A lot of the stuff we do will require email addresses. We require that you send
us an email that is associated with your institution. The following are the
currently approved domains:

| Domain                   | Institution                                                                         |
| ------------------------ | ----------------------------------------------------------------------------------- |
| `@uci.edu`               | [University of California, Irvine](https://uci.edu/)                                |
| `@ics.uci.edu`           | [UCI Donald Bren School of Information and Computer Sciences](https://ics.uci.edu/) |
| `@students.imperial.edu` | [Imperial Valley College](https://imperial.edu/)                                    |

:::note

Don't see you domain? Let Carlos know. It just means we haven't had students
with that domain before. Remember, it has to be from an educational institution.
That is, it must end with `.edu`.

:::

## Set up your Gravatar

A lot of the services we use integrates [Gravatar](https://gravatar.com/). It
adds an icon to your email. To learn more about getting that up, please check
out this
[GitLab issue addressing it](https://gitlab.com/ojosproject/website/-/issues/20).

## Getting recognized as a member

:::tip

If you are enrolled in IN4MATX 199, you will automatically become a recognized
member.

:::

To get recognized as a member, you must contribute to the project in some way.
That may mean writing research notes, adding a commit message to any of our git
repos, etc. As long as we see you are dedicated to the project, you may get
recognized as a member if you so wish.

Benefits include...

1. Getting added to [the website](https://ojosproject.org/#team)
2. Getting access to our git repos as a Developer
3. Getting vouched for by Carlos if you need a reference
4. Probably more that I can't think of right now

## What's next?

There may be more instructions depending on the team you're in. Read the
instructions for the team you are in:

- [Developers team](/url/developers/getting-started/)
- [Engineering team](/url/engineering/getting-started/)

## Admin Checklist

This checklist is here for the people helping you get set up.

- [ ] Send them necessary calendar invites
- [ ] Give access to the Google Drive
- [ ] Give access to Slack
- [ ] Ensure we have their email
- [ ] Ensure their Gravatar is set
- [ ] Check if they automatically are a recognized member
